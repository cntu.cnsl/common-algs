# common-algs

A Package for providing common algorithms

## Why do you need common-als
The common algorithms project aims at providing a virsualized output based on user's input with well-known algorithms.
Sometimes, it is hard to understand the algorithm exlained in the book. The project aims at virsualizing step-by-step
process of given algorithm.

## Project Status:
* Add common algorithms [] -- On going
* Add automation testing in GitLab [v]
* Add virsualize code [] -- Boxer, Ploter
* Add published code in Gitlab CI/CD [x]

## Contribution to the project
The project uses poetry to build and tests the coding. Please take a look at poetry project first.
The code structure as follows:
### common-algs/<algorithm_name>.py
* Each category of the algorithm are added in this folder (e.g. search.py includes all algorithms for searching, sort.py includes the algorithms for sorting)
* Inside each algorithm file, please add category name as class and the name of algorithm without category name as method. e.g. The Sorts are is category name and shell (not shell_sort) as method

### tests/test_<algorithm_name>_algs.py
* For each category test, please make sure that those tests cover all the implemented code (100% coverage)

### scripts/test.sh
Please execute this file before commit to GitLab to ensure that all the poetry test are passed
