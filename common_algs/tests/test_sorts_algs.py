from common_algs import __version__
from common_algs.sorts import Sorts


def test_version():
    assert __version__ == '0.1.0'


def test_basic():
    sort = Sorts([])
    assert sort.getType() == "Sorts"


def test_shell_sort():
    sort = Sorts([1, 3, 2, 4])
    assert sort.shell() == [1, 2, 3, 4]
    sort.setList([1, 2, 4, 8, 9, 10, 3])
    assert sort.getList() == [1, 2, 4, 8, 9, 10, 3]
    assert sort.shell() == [1, 2, 3, 4, 8, 9, 10]


def test_insert_sort():
    sort = Sorts([1, 3, 2, 4])
    assert sort.insert() == [1, 2, 3, 4]
    sort.setList([1, 2, 4, 8, 9, 10, 3])
    assert sort.insert() == [1, 2, 3, 4, 8, 9, 10]


def test_bubble_sort():
    sort = Sorts([1, 3, 2, 4])
    assert sort.bubble() == [1, 2, 3, 4]
    sort.setList([1, 2, 4, 8, 9, 10, 3])
    assert sort.bubble() == [1, 2, 3, 4, 8, 9, 10]
    sort.setList([])
    assert sort.bubble() == []  # abnormal


def test_selection_sort():
    sort = Sorts([1, 3, 2, 4])
    assert sort.selection() == [1, 2, 3, 4]
    sort.setList([1, 2, 4, 8, 9, 10, 3])
    assert sort.selection() == [1, 2, 3, 4, 8, 9, 10]
    sort.setList([])
    assert sort.selection() == []  # abnormal


def test_merge_sort():
    sort = Sorts([1, 3, 2, 4])
    assert sort.merge() == [1, 2, 3, 4]
    sort.setList([1, 2, 4, 8, 9, 10, 3])
    assert sort.merge() == [1, 2, 3, 4, 8, 9, 10]
    sort.setList([])
    assert sort.merge() == []  # abnormal
