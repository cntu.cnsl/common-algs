from graphics.boxer import Boxer
from graphics.ploter import Ploter


def test_box():
    test = Boxer([1, 2, 3, 4, 5])
    assert test.setup() == 1
    test = Boxer([1, 2, 3], xchange="[1,2]")
    assert test.setup() == 1
    test = Boxer([])
    assert test.setup() == 1
    test = Boxer([None])
    assert test.setup() == 1
    test = Boxer(["fdsafsa"])
    assert test.setup() == 1


def test_plot():
    test = Ploter([1, 2, 3])
    assert test.setup() == 1
    test = Ploter([])
    assert test.setup() == 0
    test = Ploter([None])
    assert test.setup() == 0
    test = Ploter(["fdsafsa"])
    assert test.setup() == 0
