from common_algs.search import Search


def test_basic():
    search = Search([])
    assert search.getType() == "Search"


def test_linear_search():
    search = Search([1, 3, 2, 4])
    assert search.linear(3) == [True, 1]
    search.setList([1, 2, 4, 8, 9, 10, 3, 8])
    assert search.getList() == [1, 2, 4, 8, 9, 10, 3, 8]
    assert search.linear(8) == [True, 3]
    search.setList([1, 2, 4, 28, 19, 10, 3, 38])
    assert search.linear(8) == [False, None]


def test_binary_search():
    search = Search([1, 3, 2, 4])
    assert search.binary(3) == [True, 1]
    search.setList([1, 2, 4, 8, 9, 10, 3, 8])
    assert search.getList() == [1, 2, 4, 8, 9, 10, 3, 8]
    assert search.binary(8) == [True, 3]
    search.setList([1, 2, 4, 28, 19, 10, 3, 38])
    assert search.binary(8) == [False, None]


def test_intepolation_search():
    search = Search([1, 2])
    assert search.intepolation(1) == [True, 0]
    search.setList([1, 2, 3, 4])
    assert search.getList() == [1, 2, 3, 4]
    assert search.intepolation(4) == [True, 3]
    assert search.intepolation(3) == [True, 2]
    search.setList([1, 2, 3, 4, 8, 9, 10])
    assert search.intepolation(8) == [True, 4]
    search.setList([1, 2, 3, 4, 8, 9, 10])
    assert search.intepolation(1) == [True, 0]
    search.setList([1, 2, 3, 4, 10, 15, 20])
    assert search.intepolation(8) == [False, None]
