class Virsualizer(object):
    _content = ""

    def print_content(self) -> bool:
        print(self._content)
        return True
