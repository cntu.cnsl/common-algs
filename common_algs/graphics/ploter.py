from graphics.virsualizer import Virsualizer


class Ploter(Virsualizer):
    """ Plot the diagram of number list.

    Represent the number as plot.
    Example:
       6 +
       5 +       |
       4 +   |   |
       3 +   |   | |
       2 +   | | | |
       1 + | | | | |
         +_1_4_2_5_3___
         |   x     x  |   Comparison(c)/Exchange(x)
    This feature is designed to use for list with large number of elements
    """
    def __init__(self, input: list):
        self.input = input

    def setup(self) -> bool:
        EMPTY_SPACE = " "
        if not self.input:
            # No input
            print("There is no input value")
            return False
        try:
            max_y = max(self.input)
            max_x = len(self.input)
            content = ""
            for y_axis in range(max_y)[::-1]:
                x_axis = ""
                for value in self.input:
                    # Column value for each row
                    if y_axis < value:
                        x_axis += f"|{EMPTY_SPACE}"
                    else:
                        x_axis += f"{EMPTY_SPACE}"*2
                content += f"{y_axis+1} +  {x_axis} \n"
            content += f"{EMPTY_SPACE}"*2 + "+" + "_"*(max_x-1) + "_".join([str(input) for input in self.input])
            self._content = content
            return True
        except TypeError:
            print("Expect list of numbers")
        return False

    def update(self, input: list) -> bool:
        self.input = input
        self.setup()
        return True
