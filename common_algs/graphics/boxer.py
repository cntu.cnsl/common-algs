from graphics.virsualizer import Virsualizer


class Boxer(Virsualizer):
    """ A graphics version of a list.

    Draw a box with number inside
    Example: [1,2,3]
       |-----|-----|-----|
       |  1  |  2  |  3  |
       |-----|-----|-----|
       |  x           x  | Comparison (c) / Exchange (x)

    For better representation, use this render if the list has less then 10 elements.
    """
    def __init__(self, input: list, list_size=10, xchange=[int], xchange_type="x") -> None:
        self.input = input
        self.input_len = len(input)
        self.list_size = list_size
        self.xchange = xchange
        self.xchange_type = xchange_type

    def setup(self) -> bool:
        content = []
        content.append(self.__setupHeaderFooter())
        content.append(self.__setupValue())
        content.append(self.__setupHeaderFooter(True))
        self._content = "\n".join(content)
        return True

    def update(self, input: list) -> bool:
        self.input = input
        self.setup()
        return True

    def __setupHeaderFooter(self, xchange_flag=False) -> str:
        content = []
        line = ""
        for val in self.input:
            val_len = len([x for x in str(val)])
            mid_spaces = (self.list_size - val_len) // 2
            if not line:
                line += "|" + "-"*mid_spaces*2 + "-"*val_len + "|"
            else:
                line += "-"*mid_spaces*2 + "-"*val_len + "|"
        # Add exchange information, only marks for 2 values
        content.append(line)
        if not content:
            return ""
        if xchange_flag and len(self.xchange) == 2 and not any([val > len(self.input) for val in self.xchange]):
            average_len = int(len(content[0]) // len(self.input))
            mark_line = "|"
            order = 0
            for index, val in enumerate(self.input):
                if index + 1 not in self.xchange:
                    mark_line += " " * average_len
                    continue
                order += 1
                half_spaces = int((average_len - len(self.xchange_type)) // 2)
                if order == 1:
                    # First mark
                    mark_line += " " * half_spaces + self.xchange_type + " " * (half_spaces) + " "
                else:
                    # End of mark
                    mark_line += " " * half_spaces + self.xchange_type + " " * (half_spaces) + "|"
            content.append(mark_line)
        return "\n".join(content)

    def __setupValue(self) -> str:
        content = ""
        for val in self.input:
            val_len = len([x for x in str(val)])
            mid_spaces = (self.list_size - val_len) // 2
            if not content:
                content += "|" + " "*mid_spaces + f"{val}" + " "*mid_spaces + "|"
            else:
                content += " "*mid_spaces + f"{val}" + " "*mid_spaces + "|"
        return content
