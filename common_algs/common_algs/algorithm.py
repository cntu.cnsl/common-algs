class Algorithm(object):
    _type: str = ""
    _input: list = []

    def getType(self) -> str:
        return self._type

    def getList(self) -> list:
        return self._input

    def setList(self, input: list) -> None:
        self._input = input
