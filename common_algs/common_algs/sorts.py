from common_algs.algorithm import Algorithm


class Sorts(Algorithm):
    """ A collection of sorts algorithms

    Args:
        input: List a list of integer number for sort

    Returns:
        List
    """
    def __init__(self, input: list) -> None:
        self._type = "Sorts"
        self._input = input

    def shell(self) -> list:
        input = self._input
        print(f"\n Original list: {input} \n")
        gap = len(input) // 2
        while(gap > 0):
            for right_index in range(gap, len(input)):
                right_value = input[right_index]
                tmp_index = right_index
                # If the value in the left is greater than in the right
                while tmp_index >= gap and  \
                        input[tmp_index - gap] > right_value:
                    input[tmp_index] = input[tmp_index - gap]
                    tmp_index -= gap
                input[tmp_index] = right_value
                print(f"Updated: \n {input} \n")
            gap //= 2
        print("Result:")
        return input

    def insert(self) -> list:
        input = self._input
        print(f"\n Original list: {input} \n")
        for next_pt in range(1, len(input)):
            current_pt = next_pt - 1
            next_value = input[next_pt]
            #
            # if the current pointer's value is greater than next value
            # we will move the current pointer down until it smaller than
            # the next value
            #     While move the current pointer to the left, we move
            #     the traversed value to the next index
            #
            while(input[current_pt] > next_value) and (current_pt >= 0):
                input[current_pt + 1] = input[current_pt]
                current_pt -= 1
            input[current_pt + 1] = next_value
            print(f"Updated: \n {input} \n")
        print("Result:")
        return input

    def bubble(self) -> list:
        input = self._input
        print(f"\n Original list: {input} \n")
        last_element_index = len(input) - 1
        for pass_number in range(last_element_index, 0, -1):
            for index in range(pass_number):
                if input[index] > input[index+1]:
                    input[index], input[index+1] = input[index+1], input[index]
                    print(f"Updated: \n {input} \n")
        print("Result:")
        return input

    def selection(self) -> list:
        input = self._input
        print(f" \n Original list: {input} \n")
        last_element_index = len(input) - 1
        for pass_number in range(last_element_index, 0, -1):  # similar to bubble sort
            max_index = 0
            for check in range(1, pass_number + 1):
                if input[check] > input[max_index]:
                    max_index = check
            input[pass_number], input[max_index] = input[max_index], input[pass_number]
        print("Result:")
        return input

    def __merge(self, input: list):
        if(len(input) > 1):
            mid_point = len(input) // 2
            left = input[:mid_point]
            right = input[mid_point:]
            print(f"Merging left: {left}")
            self.__merge(left)
            print(f"Merging right: {right}")
            self.__merge(right)

            left_pt = 0
            right_pt = 0
            merged_pt = 0
            print(f"Original Input: {input}")

            while left_pt < len(left) and right_pt < len(right):
                # Put the smaller number from each side into the merged list
                if left[left_pt] <= right[right_pt]:
                    input[merged_pt] = left[left_pt]
                    left_pt += 1
                else:
                    input[merged_pt] = right[right_pt]
                    right_pt += 1
                merged_pt += 1

            # Put larger values into merged list
            while left_pt < len(left):
                input[merged_pt] = left[left_pt]
                left_pt += 1
                merged_pt += 1
                print(f"After left merged: {input}")

            while right_pt < len(right):
                input[merged_pt] = right[right_pt]
                right_pt += 1
                merged_pt += 1
                print(f"After right merged: {input}")

        print(f"Merged: {input}")

    def merge(self) -> list:
        input = self._input
        print(f"\n Original list: {input} \n")
        self.__merge(input)
        return input
