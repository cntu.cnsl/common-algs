from common_algs.algorithm import Algorithm


class Search(Algorithm):

    """ Collection of search algorithms

    Args:
        input: List a list of integer number for searching
        item: Number for search

    Return:
        list: A list of two values [bool, int]
            bool: Return if the value is found (True) or not(False)
            int: Return the index of first matched

    """
    def __init__(self, input: list) -> None:
        self._type = "Search"
        self._input = input

    def linear(self, item: int) -> list:
        input = self._input
        found = False
        position = None
        for index, value in enumerate(input):
            if value == item:
                found = True
                position = index
                break
        return [found, position]

    def binary(self, item: int) -> list:
        input = self._input
        found = False
        position = None
        low_idx = 0
        high_idx = len(input) - 1
        while low_idx < high_idx:
            middle_idx = (high_idx - low_idx) // 2
            if input[middle_idx] == item:
                found = True
                position = middle_idx
                break
            if input[middle_idx] > item:
                high_idx = middle_idx - 1
            else:
                low_idx = middle_idx + 1
        return [found, position]

    def intepolation(self, item: int) -> list:
        # Sort
        input = self._input
        input.sort()
        found = False
        position = None
        low_idx = 0
        high_idx = len(input) - 1
        middle_idx = -1
        while low_idx < high_idx and item >= input[low_idx] and item <= input[high_idx]:
            middle_idx = low_idx + int(((float(high_idx - low_idx)/(input[high_idx] - input[low_idx])) * (
                item - input[low_idx])))
            if input[middle_idx] == item:
                print(f"Found {item} in position {middle_idx} in {input}")
                found = True
                position = middle_idx
                return [found, position]
            if input[middle_idx] > item:
                # input value is smaller than the value in the middle point
                # Because of that, we will reset the range
                # from lower_idx to the index before middle_idx
                #
                # @TODO: need test case for this branch. It seem like
                # middle_idx will never go to this branch
                high_idx = middle_idx - 1
            else:
                # input value is greater than the value in the middle point
                # do the opposite action
                low_idx = middle_idx + 1
        print(f"Not Found {item} in {input}")
        return [found, position]
