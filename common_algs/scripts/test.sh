#!/bin/bash
poetry run pytest
mypy .
poetry run flake8
poetry run coverage run -m --source=common_algs pytest tests
poetry run coverage report --show-missing
